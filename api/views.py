import base64

from django.shortcuts import get_object_or_404
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView

from tinyurl.models import Record

from .serializers import RecordSerializer

class RecordView(viewsets.ViewSet):

    renderer_classes = [JSONRenderer]

    def list(self, request):
        queryset = Record.objects.all()
        serializer = RecordSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        queryset = Record.objects.all()
        record = get_object_or_404(queryset, pk=pk)
        serializer = RecordSerializer(record)
        return Response(serializer.data)

    def create(self, request):
        record = request.data.get('record')
        # Create an record from the above data
        serializer = RecordSerializer(data=record)
        if serializer.is_valid(raise_exception=True):
            record_saved = serializer.save()
        id = bytes(str(record_saved.id).encode("utf-8"))
        b64 = base64.urlsafe_b64encode(id)
        tiny_url = "%s%s" % (settings.HOSTNAME, b64.decode("utf-8"))
        return Response({"status": "success", "short_url": tiny_url})

