import logging

from rest_framework import serializers
from tinyurl.models import Record

logger = logging.getLogger(__name__)

class RecordSerializer(serializers.Serializer):
    url = serializers.CharField(max_length=300)

    def create(self, validated_data):
        _record, created = Record.objects.get_or_create(**validated_data)
        if created:
            logger.info("New record has been created: {}".format(str(validated_data)))
        return _record
