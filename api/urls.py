from rest_framework.routers import DefaultRouter
from .views import RecordView

app_name = "api"
router = DefaultRouter()
router.register(r'records', RecordView, basename='record')
urlpatterns = router.urls