FROM ubuntu:18.04
WORKDIR /home/
# install git
RUN apt-get update && apt-get install -y git python3-pip

RUN git clone https://gitlab.com/grigoriev.204/urlshortener.git
RUN ls -la /home/urlshortener
RUN pip3 install -r ./urlshortener/requirements.txt
WORKDIR /home/urlshortener
EXPOSE 8000
STOPSIGNAL SIGTERM
RUN /usr/bin/python3 manage.py makemigrations && /usr/bin/python3 manage.py migrate
ENTRYPOINT ["/usr/bin/python3", "manage.py", "runserver", "0.0.0.0:8000"]