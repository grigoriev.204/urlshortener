
new Vue ({
  el: "#app",

  data: function() {
    return {
      url: null,
      shortUrl: null
    }
  },

  methods: {

    onSubmit: function() {
      const POSTURL = "http://127.0.0.1:8000/api/records/";
      axios.post(POSTURL, {record: {url: this.url}}, {
        "headers": {
          "X-CSRFToken": this.$root.getCookie("csrftoken")
        }
      })
      .then( res => {
        this.shortUrl = res.data.short_url;
      })
      .catch( res => {
        const { urlFieldEl } = this.$refs;
        urlFieldEl.setCustomValidity(res.data.message);
      })
    },

    onReset: function() {
      this.url = null;
      this.shortUrl = null;
    },

    getCookie: function(name) {
      let cookieValue = null;
      if (document.cookie && document.cookie !== '') {
          const cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
              const cookie = cookies[i].trim();
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) === (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
    }
  }
})