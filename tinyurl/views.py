import base64

from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse, HttpResponseNotFound
from .models import Record

# Create your views here.

def __render_404(request, message):
    result = render(request, "tinyurl/404.html", {"message": message})
    result.status_code = 404
    return result

def redirect_to_original(request):
    if request.path.endswith("/"):
        path = request.path[1:-1]
    else:
        path = request.path[1:]
    try:
        id = int(base64.urlsafe_b64decode(path))
        url = Record.objects.get(id=id)
        result = redirect(url.url)
    except ValueError:
        return __render_404(request, "Wrong URL shortcut.")       
    except Record.DoesNotExist:
        return __render_404(request, "This URL shortcut does not exist.")       
    except Exception:
        return __render_404(request, "Failed to redirect. The '%s' URL is incorrect." % url.url)
    return result

def index(request):
    return render(request, "tinyurl/index.html", {})
