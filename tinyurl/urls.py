from django.urls import path, re_path
from . import views

app_name = "tinyurl"
urlpatterns = [
    re_path(r'[A-Za-z0-9]+\={0,2}', views.redirect_to_original, name='redirect_to_original'),
    path('', views.index, name='index')
]