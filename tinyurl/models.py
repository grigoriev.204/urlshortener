from django.db import models

# Create your models here.

class Record(models.Model):
    
    url = models.CharField(max_length=300, db_index=True, unique=True)

    def __str__(self):
        return self.url