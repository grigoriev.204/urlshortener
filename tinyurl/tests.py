import base64

from django.test import TestCase, Client
from django.conf import settings

# Create your tests here.

class TestEndpoints(TestCase):

    def setUp(self):
        self.__client = Client()
    
    def test_index(self):
        response = self.__client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_redirect(self):
        example_url = "https://google.com"
        data = {"record": {"url": example_url}}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        short_url = response.data["short_url"]
        self.assertEqual(response.data["status"], "success")
        response = self.__client.get(response.data["short_url"])
        self.assertRedirects(response, example_url, fetch_redirect_response=False)

    def test_short_url(self):
        example_urls = ["https://google.com",
                        "https://ya.ru",
                        "https://microsoft.com"]
        for id, url in enumerate(example_urls):
            data = {"record": {"url": url}}
            response = self.__client.post("/api/records/", data=data, content_type="application/json")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["status"], "success")
            # add one, because id counter starts from 1
            id = bytes(str(id+1).encode("utf-8"))
            b64 = base64.urlsafe_b64encode(id)
            control_url = "%s%s" % (settings.HOSTNAME, b64.decode("utf-8"))
            self.assertEqual(response.data["short_url"], control_url)

    def test_short_one_url_many_times(self):
        example_full_url = "https://google.com"
        example_short_url = "%sMQ==" % settings.HOSTNAME
        for i in range(100):
            data = {"record": {"url": example_full_url}}
            response = self.__client.post("/api/records/", data=data, content_type="application/json")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["status"], "success")
            self.assertEqual(response.data["short_url"], example_short_url)

    def test_wrong_short_url(self):
        example_url = "https://google.com"
        data = {"record": {"url": example_url}}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        response = self.__client.get(response.data["short_url"])
        self.assertRedirects(response, example_url, fetch_redirect_response=False)
        response = self.__client.get("%sMP--" % settings.HOSTNAME)
        self.assertEqual(response.status_code, 404)

    def test_wrong_data_api(self):
        data = {"record": {"url": ""}}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        self.assertEqual(response.status_code, 400)
        data = {"record": {}}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        self.assertEqual(response.status_code, 400)
        data = {}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        self.assertEqual(response.status_code, 400)
        data = {"auto": {"asd": 123}}
        response = self.__client.post("/api/records/", data=data, content_type="application/json")
        self.assertEqual(response.status_code, 400)

